<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a responsive product landing page.">
    <title>IA-AG</title>
    
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-" crossorigin="anonymous">    
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">    
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">    
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/layouts/marketing.css') }}" />
</head>
<body>









<div class="header">
    <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
        <a class="pure-menu-heading" href="">IA-AG</a>

        <ul class="pure-menu-list">
            <li class="pure-menu-item pure-menu-selected"><a href="#" class="pure-menu-link">Home</a></li>
            <li class="pure-menu-item"><a href="#" class="pure-menu-link">Tour</a></li>
            <li class="pure-menu-item"><a href="#" class="pure-menu-link">Sign Up</a></li>
        </ul>
    </div>
</div>

<div class="splash-container">
    <div class="splash">
        <h1 class="splash-head">Algoritmos Genéticos</h1>
        <p class="splash-subhead">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </p>
        <p>
            <a href="http://purecss.io" class="pure-button pure-button-primary">Get Started</a>
        </p>
    </div>
</div>

<div class="content-wrapper">
    <div class="content">
        <h2 class="content-head is-center">AMBIENTE VIRTUAL DE TREINAMENTO EM ALGORITMOS GENÉTICOS</h2>

        <div class="pure-g">
        	<div class="l-box pure-u-1">
        		<p class="is-center">        			
	        		Técnica de IA que replica os princípios da seleção natural, onde indivíduos mais adaptados possuem mais chances de sobrevivência.
        		</p>
        	</div>
        </div>
    </div>

    <div class="ribbon l-box-lrg pure-g">
        <div class="pure-u-1">

            <h2 class="content-head content-head-ribbon is-center">Inspiração Biológica</h2>

            <p class="is-center">
                Na natureza, os indivíduos se recombinam através da reprodução gerando populações com diversidades de características e até novos atributos produtos da recombinação.

				No algoritmo, os indivíduos são soluções de um problema específico em um domínio de todas as soluções possíveis. E busca-se pela solução ótima através de combinações entre soluções.
            </p>
        </div>
    </div>

    <div class="content">
        <h2 class="content-head is-center">Representação dos Parâmetros</h2>

        <div class="pure-g">
            <div class="l-box-lrg pure-u-1-3">
            </div>
            <div class="l-box-lrg pure-u-2-3">
            	<ul>
            		<li>Determinar a configuração de uma possível solução;</li>
            		<li>Identificar e delimitar o conjunto domínio em que constam as soluções;</li>
            		<li>Cromossomos: conjunto de símbolos que representam uma possível solução.</li>
            	</ul>
            </div>
        </div>

    </div>

</div>




</body>
</html>
